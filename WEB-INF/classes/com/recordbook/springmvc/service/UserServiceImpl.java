package com.recordbook.springmvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.recordbook.springmvc.dao.UserDao;
import com.recordbook.springmvc.model.User;


public class UserServiceImpl implements UserService{
	
	@Autowired
	UserDao userDao;
	
	public void register(User user) {
		System.out.println(" "+user);
		userDao.register(user);
	}
	
	public List <User> getUsers(){
		return userDao.getUsers();
	}

}
