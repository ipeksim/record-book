package com.recordbook.springmvc.service;

import java.util.List;

import com.recordbook.springmvc.model.User;

public interface UserService {
	

	void register(User user);
	
	public List <User> getUsers();

}
