package com.recordbook.springmvc.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.recordbook.springmvc.model.User;



public class UserDaoImpl implements UserDao{
	
	@Autowired
	DataSource datasource;
	@Autowired
	JdbcTemplate jdbcTemplate;

	public void register(User user) {
		String sql = "insert into users values(?,?,?,?,?)";
		jdbcTemplate.update(sql, new Object[] { user.getFirstname(), user.getLastname(), user.getBloodtype(),
				user.getPhonenumber(), user.getAddress() });
		
	}
	
	public List<User> getUsers() {
		List<User> users = (List<User>) jdbcTemplate.query("select * from users",
			new UserMapper());
		return users;
	
	}
}

class UserMapper implements RowMapper<User> {
	public User mapRow(ResultSet rs, int row) throws SQLException {
		User user = new User();
		user.setFirstname(rs.getString("firstname"));
		System.out.println(user.getFirstname());
		user.setLastname(rs.getString("lastname"));
		user.setBloodtype(rs.getString("bloodtype"));
		user.setAddress(rs.getString("address"));
		user.setPhonenumber(rs.getString("phonenumber"));
		return user;
	}

}
