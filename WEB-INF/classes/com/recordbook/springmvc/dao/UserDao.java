package com.recordbook.springmvc.dao;

import java.util.List;

import com.recordbook.springmvc.model.User;

public interface UserDao {
	
	public List <User> getUsers();
	void register(User user);

}
