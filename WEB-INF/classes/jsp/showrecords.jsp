<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Spring MVC</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<style>
</style>
</head>
<body style="background-color:#dae4f5;">
	<header>
		<nav class="navbar navbar-dark navbar-expand-lg bg-dark text-white">
        	<div class="container-fluid py-1">
           	<a href="#" class="navbar-brand px-3">Record Book</a>
        	</div>
        	<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbar-rb" aria-controls="navbar-rb" aria-expanded="false" aria-label="Toggle navigation">
            	<span class="navbar-toggler-icon"></span>
        	</button>

        	<div class="collapse navbar-collapse" id="navbar-rb">
            	<ul class="navbar-nav me-auto mb-2 mb-lg-0">
                	<li class="nav-item px-3">
                    	<a class="nav-link active" aria-current="page" href="/MyRecordBook" class="nav-link text-white">Home</a>
                	</li>
                	<li class="nav-item px-3">
                    	<a class="nav-link active" aria-current="page" href="register" class="nav-link text-white">Registration</a>
                	</li>
                	<li class="nav-item px-3">
                    	<a class="nav-link active" aria-current="page" href="showrecords" class="nav-link text-white">Users</a>
                	</li>
            	</ul>
        	</div>
    	</nav>
	 </header>
	<div class="container gradient custom">
		<div class="row p-5">
				<c:if test="${not empty msg}">
					${msg}
				</c:if>
				<c:choose>
				<c:when test="${users != null}">
				 <h3 class="text-center mb-3">List of Users</h3>
			<div class="col border border-1 border-dark p-1 md-12 bg-light">

				<table class="table table-hover table-borderless">
					<thead class="table table-dark table-borderless">
						<tr>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Blood Type</th>
							<th>Address</th>
							<th>Phone Number</th>
						</tr>
					</thead>

					<tbody class="table table-bordered border-dark">
						<c:forEach var="user" items="${users}">
						<tr>
							<td>${user.firstname}</td>
							<td>${user.lastname}</td>
							<td>${user.bloodtype}</td>
							<td>${user.address}</td>
							<td>${user.phonenumber}</td>
						</tr>
					</c:forEach>
					</tbody>

				<tfoot></tfoot>	
				</table>
				</c:when>
				<c:otherwise>
        		No User found in the Database..
        		</c:otherwise>
				</c:choose>


			</div>
		</div>
	<footer>
    	<div class=" fixed-bottom">
    		 <div class="footer-copyright text-center py-1 bg-dark">text

  			</div>
    	
    	
    	</div>

    	</footer>
	   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>