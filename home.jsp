<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<style>
</style>
</head>
<body  style="background-color:#dae4f5;" >
	<header>
		<nav class="navbar navbar-dark navbar-expand-lg bg-dark text-white">
        	<div class="container-fluid py-1">
           	<a href="#" class="navbar-brand px-3">Record Book</a>
        	</div>
        	<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbar-rb" aria-controls="navbar-rb" aria-expanded="false" aria-label="Toggle navigation">
            	<span class="navbar-toggler-icon"></span>
        	</button>

        	<div class="collapse navbar-collapse" id="navbar-rb">
            	<ul class="navbar-nav me-auto mb-2 mb-lg-0">
                	<li class="nav-item px-3">
                    	<a class="nav-link active" aria-current="page" href="/MyRecordBook" class="nav-link text-white">Home</a>
                	</li>
                	<li class="nav-item px-3">
                    	<a class="nav-link active" aria-current="page" href="register" class="nav-link text-white">Registration</a>
                	</li>
                	<li class="nav-item px-3">
                    	<a class="nav-link active" aria-current="page" href="showrecords" class="nav-link text-white">Users</a>
                	</li>
            	</ul>
        	</div>
    	</nav>
	 </header>
    
    
     <section>
     
		<div class="container">
			<div class= "row fixed-center mt-5 pt-5 mx-auto">
				<div class="col">
					<div class="row center-align">

						<svg xmlns="http://www.w3.org/2000/svg" width="160" height="160" fill="currentColor" class="bi bi-person-plus-fill" viewBox="0 0 16 16">
            			<path d="M1 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
            			<path fill-rule="evenodd" d="M13.5 5a.5.5 0 0 1 .5.5V7h1.5a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0V8h-1.5a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z"/>
          				</svg>

					</div>
					<div class="row center-align p-3 md-4 ms-2 ">
							<a class="btn btn-outline-dark bg-light btn-lg" href="register" role="button">Registration</a>
					</div>
	
					</div>
				

				<div class="col">
					<div class="row center-align">
						<svg xmlns="http://www.w3.org/2000/svg" width="160" height="160" fill="currentColor" class="bi bi-person-lines-fill" viewBox="0 0 16 16">
  						<path d="M6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-5 6s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zM11 3.5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5zm.5 2.5a.5.5 0 0 0 0 1h4a.5.5 0 0 0 0-1h-4zm2 3a.5.5 0 0 0 0 1h2a.5.5 0 0 0 0-1h-2zm0 3a.5.5 0 0 0 0 1h2a.5.5 0 0 0 0-1h-2z"/>
						</svg>

					</div>
					<div class="row center-align p-3 md-4 ms-2">
							<a class="btn btn-outline-dark bg-light btn-lg" href="showrecords" role="button">Users</a>
	
					</div>
				</div>
			</div>
			</div>

		</div>

    	<footer>
    	<div class=" fixed-bottom">
    		 <div class="footer-copyright text-center py-1 bg-dark">text

  			</div>
    	
    	
    	</div>

    	</footer>
		
    </section>




	   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>
